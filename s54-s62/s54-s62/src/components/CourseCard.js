import {Row, Col, Card, Button} from 'react-bootstrap';


export default function CourseCard() {
    return (
        <Row>
            <Col xs={12}>
                <Card id='courseComponent1'>
                    <Card.Body>
                        <Card.Title>JavaScript</Card.Title>
                        <Card.Text>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <p>JavaScript is a lightweight interpreted or just-in-time compiled programming language with first-class functions.</p>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <p>Php 10.00</p>
                        </Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
};