import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';
// import AppNavbar from './AppNavbar';

// Import React Bootstrap/bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>
);

// ----------------------------------------------------------------------------------------------------
// This is a sample to show how react dynamically renders its page. 
// const name = "john Doe";

// const user = {
//   firstName: 'Jane',
//   lastName: "Smith"
// }

// function userName(user) {
//   return user.firstName + ' ' + user.lastName
// }

// const element = <h1>Hello, {userName(user)}</h1>



// // render the values or data from the element and render it to the root element/component root is from the index.html 
// root.render(element);
